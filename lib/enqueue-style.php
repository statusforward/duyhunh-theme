<?php
/*********************
Enqueue the proper CSS
if you use Sass.
*********************/
if( ! function_exists( 'duyhuynh_enqueue_style' ) ) {
	function duyhuynh_enqueue_style()
	{
		// foundation stylesheet
		wp_register_style( 'foundation-stylesheet', get_stylesheet_directory_uri() . '/assets/css/app.css', array(), '' );

		// Register the main style
		wp_register_style( 'revslider-stylesheet', get_stylesheet_directory_uri() . '/assets/css/revslider.css', array(), '', 'all' );
		wp_register_style( 'default-stylesheet', get_stylesheet_directory_uri() . '/assets/css/style.css', array(), '', 'all' );
		wp_register_style( 'custom-stylesheet', get_stylesheet_directory_uri() . '/assets/css/custom.css', array(), '', 'all' );
		wp_register_style( 'iconfonts-stylesheet', get_stylesheet_directory_uri() . '/assets/ico/foundation-icons/foundation-icons.css', array(), '', 'all' );
		
		wp_enqueue_style( 'foundation-stylesheet' );
		wp_enqueue_style( 'default-stylesheet' );
		wp_enqueue_style( 'custom-stylesheet' );
		wp_enqueue_style( 'iconfonts-stylesheet' );
		wp_enqueue_style( 'revslider-stylesheet' );
		
	}
}
add_action( 'wp_enqueue_scripts', 'duyhuynh_enqueue_style' );
?>