<?php
//////////////////////////////////////////////////////////////
// CUSTOM FUNCTIONS FILE AFER FUNCTIONS.PHP
/////////////////////////////////////////////////////////////


////////////////////////////////////////////////
/*** Enqueue a IE-specific style sheet. ***/
////////////////////////////////////////////////
add_action( 'wp_enqueue_scripts', 'prefix_add_ie8_style_sheet', 200 );
function prefix_add_ie8_style_sheet() {
	global $wp_styles;	
	wp_enqueue_style( 'ie8-stylesheet', get_stylesheet_directory_uri() . '/assets/css/ie8.css', array(), '1.0.0' );
	$wp_styles->add_data( 'ie8-stylesheet', 'conditional', 'IE 8' );
}



////////////////////////////////////////////////
//Activate Galleriffic Script
////////////////////////////////////////////////
function galleriffic_js()   
{ ?>
<?php if (is_singular( 'artwork' )) { ?>
<!-- Galleriffic Script-->
<script type="text/javascript">
jQuery(document).ready(function($){
	// We only want these styles applied when javascript is enabled
	$('div.content').css('display', 'block');

	
	$('#thumbs').galleriffic({
		imageContainerSel: '#slideshow',
		controlsContainerSel: '#controls',
		captionContainerSel: '#caption',
		loadingContainerSel: '#loading',
		prevLinkText:              '&lsaquo;',
		nextLinkText:              '&rsaquo;',
		nextPageLinkText:          'Next',
		prevPageLinkText:          'Previous',
		autoStart:                 false,
		delay:                     5000,
		numThumbs: 8,
		//enableHistory: true,
		preloadAhead: 40,
		onSlideChange: function(prevIndex, nextIndex) {

			// Update the photo index display
			this.$captionContainer.find('div.photo-index')
				.html(/*'Photo '+ */(nextIndex+1) +' of '+ this.data.length);
		}
	});
	
	/**************** Event handlers for custom next / prev page links **********************/

	gallery.find('a.prev').click(function(e) {
		gallery.previousPage();
		e.preventDefault();
	});

	gallery.find('a.next').click(function(e) {
		gallery.nextPage();
		e.preventDefault();
	});

	/****************************************************************************************/
	
	function pageload(hash) {
		if(hash) {
			$.galleriffic.gotoImage(hash);
		} else {
			gallery.gotoIndex(0);
		}
	}
	$.historyInit(pageload, "advanced.html");
	$("a[rel='history']").live('click', function(e) {
		if (e.button != 0) return true;
		var hash = this.href;
		hash = hash.replace(/^.*#/, '');
		$.historyLoad(hash);
		return false;
	});

	/****************************************************************************************/
});	
</script>
<?php } ?>
<?php  
}  
add_action('wp_footer', 'galleriffic_js',15);


////////////////////////////////////////////////
//Adding body classes
////////////////////////////////////////////////
function body_class_name( $classes ){
	if( is_singular() )
	{
		global $post;
		array_push( $classes, "{$post->post_type}-{$post->post_name}" );
	}
	return $classes;
}

add_filter( 'body_class', 'body_class_name' );

function body_class_name_interior( $interior_classes ){
	if( !is_front_page() )
	{
		global $post;
		array_push( $interior_classes, "interior" );
	}
	return $interior_classes;
}

add_filter( 'body_class', 'body_class_name_interior' );



//////////////////////////////////////////////////////////////
// Custom Post Types and Custom Taxonamies
/////////////////////////////////////////////////////////////
add_action( 'init', 'create_post_types' );
function create_post_types() {
	
	$labels = array(
		'name' => __( 'Artwork' ),
		'singular_name' => __( 'Artwork' ),
		'add_new' => __( 'Add New' ),
		'add_new_item' => __( 'Add New Artwork' ),
		'edit' => __( 'Edit' ),
		'edit_item' => __( 'Edit Artwork' ),
		'new_item' => __( 'New Artwork' ),
		'view' => __( 'View Artwork' ),
		'view_item' => __( 'View Artwork' ),
		'search_items' => __( 'Search Artwork' ),
		'not_found' => __( 'No Artwork Found' ),
		'not_found_in_trash' => __( 'No Artwork found in Trash' ),
		'parent' => __( 'Parent Artwork' ),
	);
	
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,		
		'rewrite' => array( 'slug' => 'artwork', 'hierarchical' => true, 'with_front' => false ),
		'capability_type' => 'post',
		'hierarchical' => true,
		'capability_type' => 'page',
		'menu_position' => null,
		'supports' => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'excerpt')
	); 	
	
	register_post_type( 'artwork' , $args );
	flush_rewrite_rules( false );
}



////////////////////////////////////////////////
/* Deregister Revolution Slider Styles*/
////////////////////////////////////////////////

add_action( 'wp_print_styles', 'my_deregister_styles', 100 );  
function my_deregister_styles() {
    wp_deregister_style( 'rs-plugin-settings' ); //  
}





////////////////////////////////////////////////
//Add MailChimp optin for contact form
////////////////////////////////////////////////

function wpcf7_send_to_mailchimp($cfdata) {

$formtitle = $cfdata->title;
$formdata = $cfdata->posted_data;

if( $formdata['mailchimp-opt-in'] ) {

$send_this_email = $formdata['email'];
$mergeVars = array(
'FNAME'=>$formdata['first-name'],
'LNAME'=>$formdata['last-name']
);

// MCAPI.class.php needs to be in theme folder
require_once('MCAPI.class.php');

// grab an API Key from http://admin.mailchimp.com/account/api/
$api = new MCAPI('48f6adbcd5e67916d7e562a6d2ce1f2f-us6');

// grab your List's Unique Id by going to http://admin.mailchimp.com/lists/
// Click the "settings" link for the list - the Unique Id is at the bottom of that page.
$list_id = "fe2f03c435";

// Send the form content to MailChimp List without double opt-in
$retval = $api->listSubscribe($list_id, $send_this_email, $mergeVars, 'html', false);

} // end if

}
add_action('wpcf7_mail_sent', 'wpcf7_send_to_mailchimp', 1);

/*-----------------------------------------------------------------------------------*/
/* Remove Unwanted Admin Menu Items */
/*-----------------------------------------------------------------------------------*/

function remove_admin_menu_items() {
	$remove_menu_items = array(__('Comments'),__('Posts'));
	global $menu;
	end ($menu);
	while (prev($menu)){
		$item = explode(' ',$menu[key($menu)][0]);
		if(in_array($item[0] != NULL?$item[0]:"" , $remove_menu_items)){
		unset($menu[key($menu)]);}
	}
}

add_action('admin_menu', 'remove_admin_menu_items');


////////////////////////////////////////////////
//Google Analytics
////////////////////////////////////////////////
function my_analytics()   
{ ?> 
<!-- Google Analytics --> 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55990633-1', 'auto');
  ga('send', 'pageview');

</script>

<?php  
}  
add_action('wp_footer', 'my_analytics',20);




function my_rev_transition()
{ ?>
<?php if (is_front_page()) { ?>
<script>
jQuery('.rev_slider_wrapper ul li').each(function() {
    jQuery(this).data('transition','fade');
});
</script>
<?php } ?>
<?php
}
add_action('wp_footer', 'my_rev_transition',20);


?>