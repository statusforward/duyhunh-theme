<?php

/**********************
Enqueue CSS and Scripts
**********************/

// loading modernizr and jquery, and reply script
if( ! function_exists( 'duyhuynh_enqueue_scripts' ) ) {
	function duyhuynh_enqueue_scripts() {
	  if (!is_admin()) {

	    // register scripts
	    wp_register_script( 'duyhuynh_galleriffic', get_template_directory_uri() . '/assets/js/galleriffic/jquery.galleriffic.js', array(), '', false );
	    wp_register_script( 'duyhuynh_jquery-history', get_template_directory_uri() . '/assets/js/galleriffic/jquery.history.js', array(), '', false );	    
	    wp_register_script( 'duyhuynh_jquery-rollover', get_template_directory_uri() . '/assets/js/galleriffic/jquery.opacityrollover.js', array(), '', false );
	    //wp_register_script( 'topbar', get_template_directory_uri().'/bower_components/foundation/js/foundation/foundation.topbar.js', null, '1.0', true);


	    // enqueue scripts
	    wp_enqueue_script( 'duyhuynh_galleriffic' );
	    wp_enqueue_script( 'duyhuynh_jquery-history' );
	    wp_enqueue_script( 'duyhuynh_jquery-rollover' );
	    //wp_enqueue_script( 'topbar' );
	  }
	}
}
add_action( 'wp_enqueue_scripts', 'duyhuynh_enqueue_scripts' );


?>