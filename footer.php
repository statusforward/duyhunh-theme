<?php if (!is_front_page()) {?>
	</div><!-- Row End -->
</div><!-- Container End -->
<?php } ?>

<footer class="full-width" role="contentinfo">
	<div class="row">
		<div class="medium-4 columns mailchimp">
			<h5>Newsletter Sign-Up</h5>
			<!-- Begin MailChimp Signup Form -->
			<form action="//duyhuynh.us6.list-manage.com/subscribe/post?u=b4b3bf801df8a042014889278&amp;id=fe2f03c435" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
			<div class="row">
			    <div class="medium-12 columns">
			      <div class="row collapse">
			        <div class="medium-10 columns">
			          <input type="email" value="" placeholder="Enter email address" name="EMAIL" class="required email" id="mce-EMAIL">
			        </div>
			        <div class="medium-2 columns">
			          <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button radius">
			        </div>
			        <div id="mce-responses" class="clear">
						<div class="response error" id="mce-error-response" style="display:none"></div>
						<div class="response error" id="mce-success-response" style="display:none"></div>
					</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					<div style="position: absolute; left: -5000px;"><input type="text" name="b_b4b3bf801df8a042014889278_fe2f03c435" tabindex="-1" value=""></div>
			      </div>
			    </div>
			 </div>
			</form>
									
			<!--End mc_embed_signup-->
		</div>
		<div class="medium-8 columns">
			<p class="copy"><strong>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></strong><br />All Rights Reserved.<br />Website by&nbsp;<a href="//www.opendoordesignstudio.com" target="_blank">ODDS</a></p>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

<script>
	(function($) {
		$(document).foundation();
	})(jQuery);
</script>
	
</body>
</html>