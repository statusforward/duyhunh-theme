<?php
/*
Template Name: Artwork
*/
get_header(); ?>

	<div class="medium-12" id="content" role="main">
	
	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
				<?php reverie_entry_meta(); ?>
			</header>
			<div class="entry-content">
				<div class="artwork-content"><?php the_content(); ?></div>
				<div id="artwork">
										
					<?php //start query for portfolio posts
						$paged = get_query_var('paged') ? get_query_var('paged') : 1;  
					    $wpbp = new WP_Query(array(  
					            'post_type' =>  'artwork',  
					            'posts_per_page'  =>'-1',
					            'order'=>'ASC',
					            'paged' => $paged
					        )  
					    );  
					?>		
					<?php if ($wpbp->have_posts()) : while  ($wpbp->have_posts()) : $wpbp->the_post(); ?>
					<?php $terms = get_the_terms( get_the_ID(), 'mediums' ); //get Project Types ?> 
					<div class="medium-4 columns" id="artwork-<?php echo $post->post_name;?>">
						
						<?php if(has_post_thumbnail()) : ?>
						<a href="<?php the_permalink(); ?>" rel="bookmark" class="artwork-category-thumb" >	
							<?php the_post_thumbnail("thumbnail", array('class' => 'thumb', 'alt' => ''.get_the_title().'', 'title' => ''.get_the_title().'')); ?>
						</a>
						<?php endif; ?>	
						
						<a href="<?php the_permalink(); ?>" rel="bookmark" ><h2 class="artwork-name"><?php the_title(); ?></h2></a>																													
					</div>
				    <?php $count++; ?>
					<?php endwhile; endif;?><?php wp_reset_query(); ?>
				</div>
			</div>
			
			
			<footer>
				<?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', 'reverie'), 'after' => '</p></nav>' )); ?>
			</footer>
		</article>
	<?php endwhile; // End the loop ?>

	</div>
		
<?php get_footer(); ?>