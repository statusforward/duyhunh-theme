<?php
/*
Template Name: Default - 2/3 Secondary Columns
*/
?>

<?php get_header(); ?>

	<div class="small-11 medium-10 small-centered columns" id="content" role="main">
	
	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
				<?php reverie_entry_meta(); ?>
			</header>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			<?php if(get_field('col1')) { ?>
			<div class="secondary-content row">
				<hr />				
				<div class="medium-5 medium-push-7 columns">				
				<?php if(get_field('col2')) { ?><?php the_field('col2'); ?><?php } ?>
				</div>
				<div class="medium-7 medium-pull-5 columns">				
					<?php the_field('col1'); ?>
				</div>
			</div>				
			<?php } ?>						
			<footer>
				<?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', 'reverie'), 'after' => '</p></nav>' )); ?>
			</footer>
		</article>
	<?php endwhile; // End the loop ?>
		 
    </div><!-- end #main -->

<?php get_footer(); ?>
