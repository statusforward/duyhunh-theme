<?php
/*
Template Name: Side Bar
*/
get_header(); ?>

<!-- Row for main content area -->
	<div class="small-12 medium-8 columns" id="content" role="main">
	
	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
				<?php reverie_entry_meta(); ?>
			</header>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			<footer>
				<?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', 'reverie'), 'after' => '</p></nav>' )); ?>
			</footer>
		</article>
	<?php endwhile; // End the loop ?>

	</div>
	<aside id="sidebar" class="small-12 medium-4 columns">
		<div class="entry-content">
			<?php if(get_field('sidebar_content')) { ?><?php the_field('sidebar_content'); ?><?php } ?>
			<?php if(is_page('contact')) { ?><div class="gmap"><iframe style="width: 100%; height: 280px;" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps?sll=35.2139226,-80.8561511&amp;sspn=0.007065051524524881,0.013239540332789124&amp;t=m&amp;q=Lark+&amp;ie=UTF8&amp;hq=Lark&amp;hnear=&amp;ll=35.213929,-80.856006&amp;spn=0.004909,0.008132&amp;z=16&amp;output=embed"></iframe></div><?php } ?>
		</div>
		<?php get_sidebar(); ?>
	</aside><!-- /#sidebar -->		
<?php get_footer(); ?>