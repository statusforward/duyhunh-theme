<?php get_header(); ?>		
	
		
	<div class="medium-12 columns" id="content" role="main">
		
		<div class="artwork-menu">
			<?php //start query for areas
				$paged = get_query_var('paged') ? get_query_var('paged') : 1;  
			    $wpbp = new WP_Query(array(  
			            'post_type' =>  'artwork',
			            'posts_per_page'  =>'-1',
			            'order'=>'ASC',
			            'paged' => $paged
			        )  
			    );  
			?>						
			<ul>
				<?php if ($wpbp->have_posts()) : while  ($wpbp->have_posts()) : $wpbp->the_post(); ?>
				<li<?php if ( $post->ID == $wp_query->post->ID ) { echo ' class="current"'; } else {} ?>><a href="<?php the_permalink(); ?>"  <?php post_class(); ?> rel="bookmark"><p><?php the_title() ?></p><span class="dash"></span></a></li>
				<?php $count++; ?>
				<?php endwhile; endif;?><?php wp_reset_query(); ?>
			</ul>					
		</div>	
			
		<?php //include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); ?>		
		<?php while (have_posts()) : the_post(); ?>			
			<!-- containers for photo, caption and controls display -->
			<?php if(get_field('artwork_images')) { ?>
			<div class="carousel-prev-next">
				<div id="controls" class="controls medium-12 columns"></div>
			</div>
			<?php } ?>
			<div class="row content image-detail">
				<?php if(!get_field('artwork_images')) { ?>
				<div class="medium-12 columns no-work">There is currently no work under <?php the_title(); ?>.</div>
				<?php } ?>
				<!-- controls -->
				<!-- title, caption and photo index -->
				<!-- photo appears here -->
				<div class="slideshow-container medium-8 medium-push-4 columns">
					<div id="loading" class="loader"></div>
					<div id="slideshow" class="slideshow"></div>
				</div>
				<div id="caption" class="caption-container medium-4 medium-pull-8 columns">					
					<div class="photo-index"></div>										
				</div>			

			</div> <!-- #end content -->
			<!-- carousel navigation -->
			<div class="navigation-container">
				<div id="thumbs" class="navigation medium-12">
					<?php $images = get_field('artwork_images'); if( $images ): ?>
					<ul class="thumbs noscript">          
				    	<?php foreach( $images as $image ): ?>
				        <li>
				            <a class="thumb" href="<?php echo $image['url']; ?>" title="<?php echo $image['title']; ?>"><img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
				            <div class="caption">
				            	<h3><?php if(get_field('custom_display_title', $image['id'])) : ?><?php echo get_field('custom_display_title', $image['id']); ?><?php else : ?><?php echo $image['title']; ?><?php endif; ?></h3>
				                <div class="product-info"><?php echo get_field('information', $image['id']); ?></div>
				                <?php if(get_field('price', $image['id'])) : ?><div class="price">$<?php echo get_field('price', $image['id']); ?>               </div><?php endif; ?>
				                <div class="purchase-buttons">				                
				                	<?php if(get_field('availability', $image['id'])) : ?>
				                	<span class="sold button radius">Sold</span>				                					                
				                	<?php elseif(get_field('seller', $image['id'])) : ?>				                	
				                		<?php if(get_field('seller', $image['id']) == "artinvention") : ?>
				                			<a href="http://artandinventiongallery.wordpress.com/art-artists/artwork/duy-huynh/" class="button radius" target="_blank">Contact Art & Invention Gallery</a>				                		

				                		<?php elseif(get_field('seller', $image['id']) == "bluespiral") : ?>
				                			<a href="http://www.bluespiral1.com/Artist-Detail.cfm?ArtistsID=796" class="button radius" target="_blank">Contact Blue Spiral 1 Gallery</a>				                		
				                		<?php elseif(get_field('seller', $image['id']) == "imprint") : ?>
				                			<a href="https://www.brumfieldgallery.com/store/c44/Duy_Huynh.html" class="button radius" target="_blank">Contact Brumfield Gallery</a>				                		
				                		<?php elseif(get_field('seller', $image['id']) == "julesplace") : ?>
				                			<a href="http://julesplace.com/artists/huynh-duy/" class="button radius" target="_blank">Contact Jules Place</a>				                		
				                		<?php elseif(get_field('seller', $image['id']) == "larkandkey") : ?>
				                			<a href="https://larkandkey.com/collections/duy-huynh" class="button radius" target="_blank">Contact Lark & Key Gallery</a>				                		
										<?php endif; ?>
																		
				                <?php else : ?>
				                					                
				                <?php endif; ?>				            	
				                </div>           
				            </div>				            
				        </li>
				        <?php endforeach; ?>
				    </ul>
				    <?php endif; ?>
				</div>	<!-- #end thumbnails -->
				
			</div><!-- #end carousel navigation-container -->				
									
		<?php endwhile; ?>
													    	
	</div><!-- #end artwork-single -->
<?php get_footer(); ?>